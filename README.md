# Assignment04

1. Login page DONE ✔ location: app/login
 - User enters username that is saved to local storage.

2. Pokemon choose page  location: app/pokemon-list
 - When pokemon card is clicked, it is saved to local storage array with the keyValue of "pokemon".

3. Profile page location: app/profile
 - pokemon chosen are saved and fetched from local storage with GET. and are placed in new grid cards dynamically.
 - User is able to release captured pokemon.
 - Not able to release single selected pokemon, but is easily implemented.

Navbar has profile, pokemon catelogue button. And logout button that is only shown if logged in.

/services/data.service.ts
HTTP request are stored here.
