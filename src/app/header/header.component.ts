import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  constructor(private router : Router) { }

  ngOnInit(): void {
    this.shownMethod();
  }

  isShown: boolean = false;

  shownMethod () { //Method that only shows logout button if user is logged in.
    if (localStorage.getItem("Users") === null) {
      this.isShown = this.isShown; 
  }
    else {
      this.isShown = ! this.isShown;
    }
  }
  // onClick that removes "Users" and reloads page.
  onLogout() {
    localStorage.removeItem("Users"); // removes "pokemon" keyValue from local storage
    window.location.reload(); //reloads page to show deleted pokemon
  }

}
