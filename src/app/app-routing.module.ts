import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { PokemonListComponent } from "./pokemon-list/pokemon-list.component";
import { ProfileComponent } from "./profile/profile.component";





const routes: Routes = [
    {   //login page redirect when empty URL
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'  
    },
    {   //myPokemon page
        path: 'profile',
        component: ProfileComponent
    },
    {   //login page
        path: 'login',
        component: LoginComponent
    },
    {   //pokemon display page
        path: 'pokemon-list',
        component: PokemonListComponent
    },
    {   //redirect to page-not-found when wrong URL
        path: '**', 
        component: PageNotFoundComponent,
    }


]

@NgModule({
    imports: [ RouterModule.forRoot(routes)],
    exports: [ RouterModule ]

})
export class AppRoutingModule {}