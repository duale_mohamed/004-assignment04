import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, NgForm } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector:'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
}) //Decorator
export class LoginComponent implements OnInit {
    
    constructor(private router: Router) {}

    ngOnInit() {
        
    }

    
    // Creates "Users" keyValue in local Storage and stores input value from HTML. redirects after successfull login.
    onSubmit(createForm: NgForm) {
        let loginInfo = localStorage.setItem('Users', JSON.stringify(createForm.value));
        let setLogin = localStorage.getItem(createForm.value)
        console.log(setLogin)
        if (loginInfo !== null) {
            this.router.navigate(['/pokemon-list']);
        }
        else {
            console.log("Error MSG")
        }
    }
} 