import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: 'pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  pokemons: any[] = [];
  page = 1;
  totalPokemons!: number;
  
  constructor(
    private dataService: DataService, private router: Router
  ) { }

  ngOnInit(): void {
    this.getPokemons();
    this.loginRedirect();
  }

  // Checks if a user is stored in local storage.
  loginRedirect () {
    if (localStorage.getItem("Users") === null) {
        this.router.navigate(['/login']);
        console.log("Logged in User not Detected."); 
    }
}
  // Selected pokemon in /pokemon-list are added to local storage and shown in profile.
  pickPokemon(event: any) {
    if (localStorage.getItem("pokemon") == null) {
      localStorage.setItem("pokemon", "[]");
    }
    
    let oldData =JSON.parse(localStorage.getItem("pokemon") || '{}');
    oldData.push(event);

    localStorage.setItem("pokemon", JSON.stringify(oldData));
    alert(event.name + " has been picked! and Added to your Profile!")
  }

  //GET pokemons method
  getPokemons() {
    this.dataService.getPokemons(115, this.page + 0 ) //limit from data.sercice.ts
    .subscribe((response: any) => {
      //this.pokemons = response.count;
      
      response.results.forEach((result : any) => {
        this.dataService.getMorePokemonData(result.name)
        .subscribe((uniqResponse: any) => {
          this.pokemons.push(uniqResponse);
          //console.log(this.pokemons) 
          // logs array of all pokemon 100 times. Because of value in line 24.
        });
      });
    });
  }

}
