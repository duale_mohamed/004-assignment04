import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    // array index is used to display in card grid
    console.log(this.loggedUser.name);
    this.loginRedirect();
  }


  // Checks if a user is stored in local storage.
  loginRedirect () {
    if (localStorage.getItem("Users") === null) {
        this.router.navigate(['/login']);
        console.log("Logged in User not Detected."); 
    }
}


  // GET username stored local storage
  loggedUser = JSON.parse(localStorage.getItem("Users") || '{}')
  // GET selected pokemon stored in local storage
  pokeData = JSON.parse(localStorage.getItem("pokemon") || '{}');
  

  // Emptys the local storage
  releasePokemonOnSubmit() {
    localStorage.removeItem("pokemon"); // removes "pokemon" keyValue from local storage
    window.location.reload(); //reloads page to show deleted pokemon
    
  }

}
